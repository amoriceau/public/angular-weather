# AngularWeather
### Author: Axel Moriceau
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Description
Get your weather data from (meta weather api)[https://www.metaweather.com/api/] in your browser

## Features
### Main page
- A main page with 4 static cities: Lyon, Paris, Toulouse and Bordeaux (in France) displayed in cards
  - the cards contains some information about the actual weather in those cities as the weather, the temperature, the humidity and so on
  - A the bottom of the page you'll find the last time the data were fetched from the api

### Detailed page
- A detailed view, click on the card to go to this page or search the city you want in the app bar
  - The detailed view contains all the information you've seen on the card and some other details about the weather
  - you'll also find the forecasts for the five next days and you're able to navigate between them by clicking on the graph at the bottom or with the arrow buttons

### Appbar
- The app bar at the top of the application contains a link to the homepage and a search bar
  - The search bar allows you to search through the available cities in the api data
  - Advanced information
    - While typing a suggestion list (clickabl every suggestion is a link) of cities that matches your input will be updated
    - if you do not enter a full city name the app will choose the first available city in the suggestion list or do nothing if there is no city that matches your name

### Cache system
- A custom cache system has been created to limit the API consumption
  - Every city you'll search will be stored in the session storage and retrieved if you want to show the data for a saved city again
  - The cache has a validity of 30min or less (if you close your session) then it will fetch the data again even if the city is already stored
  - suggestions are also cached, a request is made on your first letter input to fetch all the cities that starts with the letter, then Javascript will filter the suggestion depending on the rest of your input
  - The suggestion cache is a simple javascript Map Object that will store data as FirstLetter:string: Cities[], so if you type a 'B', delete it type a 'T' delete it and type a 'B' again no api call will be done we just retrieve the cities from the Map 

### Known bugs
- When researching a city from the details of a city the chart for the forecasts will not be rendered as wanted, the data will be ok but the bars of the chart will have the same height
  - a window.location.reload() at the end of the navigation will do the trick but it's pretty ugly so it's not implemented yet

# Known improvements
- The cache system of the session storage is based on a key named 'last_fetch', this key should be in every entries and not a global key to ensure you really need to fetch data, with the current implementation in some case it's possible to have a city with outdated data
  - workaround: cache is fully cleared every hours (a better option is to attribute a fetch date to every city when fetched)
