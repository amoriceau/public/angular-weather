/**
 * Custom mapper for weather icons names and description
 */

export const iconsMap: {[key: string]: {icon: string, title:string}} = {
  sn: {icon: "snowy", title:'snow'},
  sl: {icon: "snowy", title:'sleet'},
  h:  {icon: "hail",  title:'hail'},
  t:  {icon: "thunderstorms", title:'thunderstorm'},
  hr: {icon: "showers", title:'heavy rain'},
  lr: {icon: "drizzle", title:'light rain'},
  s:  {icon: "heavy-showers", title:'showers'},
  hc: {icon: "cloudy-2", title:'heavy clouds'},
  lcd: {icon: "sun-cloudy", title:'cloudy'},
  lcn: {icon: "moon-cloudy", title:'cloudy'},
  cd:  {icon: "sun-clear", title:'clear'},
  cn:  {icon: "moon-clear", title:'clear'},
}
