export class sessionStorageMixin {
  constructor (){
    setInterval(()=>{
      sessionStorage.clear()
    }, 1000 * 3600)
  }

    /**
   * @name lastRefresh
   * @description Return the last time the data was fetched from the API
   *
   * @returns string
   * @memberof sessionStorageMixin
   */
     lastRefresh(){
      return sessionStorage.getItem('last_fetch')
    }


    /**
     * @name isStored
     * @description Fetch storage data with the provided key, return true if the sessionStorage has data at the given index else false
     *
     * @param {number} woeid
     * @returns {boolean}
     * @memberof sessionStorageMixin
     */
    isStored(woeid: number):boolean{
      return !!sessionStorage.getItem(woeid.toString())
    }
}
