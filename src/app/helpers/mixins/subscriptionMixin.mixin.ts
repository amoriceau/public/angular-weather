import { WeatherApiService } from '../../services/weather-api/weather-api.service';
import { WeatherInformation } from 'src/interfaces/WeatherInformation.interface';
import { sessionStorageMixin } from './sessionStorageMixin.mixin';
import { City } from 'src/interfaces/City.interface';
export class subscriptionMixin extends sessionStorageMixin {
  private weatherService!: WeatherApiService;

  constructor(weatherService: WeatherApiService) {
    super();
    this.weatherService = weatherService;
  }
  /**
   * @description Will hold weatherInformation data for an array of cities
   * @usage Cities information will be stored here if getCityInfo() is called with the 'array' strategy, then you can use this array in *ngFor in your HTML file
   *
   * @type {WeatherInformation[]}
   * @memberof subscriptionMixin
   */
  cities_infos: WeatherInformation[] = [];

  /**
   * @description Will hold weatherInformation data for one city
   * @usage City information will be stored here if getCityInfo() is called with the 'sole' strategy, then you can use this variable in your HTML file
   *
   * @type {WeatherInformation}
   * @memberof subscriptionMixin
   */
  city_infos!: WeatherInformation;


  /**
   * @description Map object used to store arrays of City referenced by their first letter (e.g: t => [Tokyo, Toronto...])
   *
   * @type {Map<string, City[]>}
   * @memberof subscriptionMixin
   */
  city_list: Map<string, City[]> = new Map();


  /**
   * @name getCityInfo
   * @description Calls the API service to get the available information for a given city through it's woeid (where on earth id)
   * @returns void
   *
   * @param {number} woeid
   * @param {string} [strategy='sole']
   * @param {boolean} [force=false]
   * @param {Function} [cb=(): any => {}]
   * @memberof subscriptionMixin
   */
  getCityInfo(
    woeid: number,
    strategy: string = 'sole',
    force: boolean = false,
    cb: Function = (): any => {}
  ): void {
    // Default date provided has fallback if the sessionStorage has been manually cleared, act as a disguised 'force' in this situation
    // new Date(0) is another way to say new Date(01/01/1970) which is the timestamp 0 in unix systems
    if (
      !this.isStored(woeid) ||
      new Date(sessionStorage.getItem('last_fetch') ?? new Date(0).toDateString())
      < new Date(Date.now() - 1000 * 60 * 30) ||
      force
    ) {
      this.weatherService.getCity(woeid).subscribe((data) => {
        this.weatherService.updateCache(data);
        this.finishSubscription(data, strategy, cb);
      });
    } else {
      this.finishSubscription(
        this.weatherService.getFromCache(woeid),
        strategy,
        cb
      );
    }
  }


  /**
   * @name getCitiesList
   * @description Fills a Map object that will be accessible in the callback passed as the second param, the Map will store cities object referenced by their first letter as the key map (e.g: t => Tokyo)
   *
   * @param {string} value
   * @param {Function} cb
   * @memberof subscriptionMixin
   */
  getCitiesList(value: string, cb: Function): void {
    if (value.length === 1) {
      if (!this.city_list.has(value)) {
        this.weatherService.getCities(value).subscribe((data: any) => {
          this.city_list.set(
            value.toLowerCase(),
            // Filter is needed here because the Api send all cities that contains the first letter and not only the ones that starts with this letter
            (data as City[]).filter((city) =>
              city.title.toLowerCase().startsWith(value.toLowerCase())
            )
          );
          // We call it here and then return to ensure that cb() will be called AFTER data is mapped
          cb(value, this.city_list);
          return;
        });
      }
      cb(value, this.city_list);
    } else {
      cb(value, this.city_list);
    }
  }

  /**
   * @name finishSubscription
   * @description Fallback for getCityInfo, this method is called when data has been fetched by the subscription to update application data
   * @returns void
   *
   * @private
   * @param {WeatherInformation} data
   * @param {string} strategy
   * @param {Function} cb
   * @memberof subscriptionMixin
   */
  private finishSubscription(
    data: WeatherInformation,
    strategy: string,
    cb: Function
  ): void {
    if (strategy === 'sole') {
      this.city_infos = data;
    } else if (strategy === 'array') {
      this.cities_infos.push(data);
    }
    cb();
  }
}
