import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { WeatherInformation } from 'src/interfaces/WeatherInformation.interface';

@Injectable({
  providedIn: 'root',
})
export class WeatherApiService {

  constructor(private http: HttpClient) {}

  /**
   * @description Url of the used API
   *
   * @private
   * @type {string}
   * @memberof WeatherApiService
   */
  private readonly URL: string =
    'https://secret-ocean-49799.herokuapp.com/https://www.metaweather.com/api/';


  /**
   * @description httpOptions given to every API request
   *
   * @memberof WeatherApiService
   */
  httpOptions: {headers: HttpHeaders} = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
    }),
  };


  /**
   * @name updateCache
   * @description Update the sessionStorage used as cache for the application
   *
   * @returns void
   * @param {WeatherInformation} data
   * @memberof WeatherApiService
   */
  updateCache(data: WeatherInformation): void {
    sessionStorage.setItem('last_fetch', new Date().toString());
    const cachedData: string | null = sessionStorage.getItem(
      data.woeid.toString()
    );

    if (!cachedData) {
      sessionStorage.removeItem(data.woeid.toString());
    }
    sessionStorage.setItem(data.woeid.toString(), JSON.stringify(data));
  }


  /**
   * @name getFromCache
   * @description Retrieve data from the cache located at the given key, if the key does not exists returns an empty string as WeatherInformation
   *
   * @param {number} key
   * @returns {WeatherInformation}
   * @memberof WeatherApiService
   */
  getFromCache(key: number): WeatherInformation {
    return JSON.parse(
      sessionStorage.getItem(key.toString()) ?? ''
    ) as WeatherInformation;
  }


  /**
   * @name getCity
   * @description Consume the API to fetch the information about the city with the given woeid, returns an Observable to subscribe on
   *
   * @param {number} id
   * @returns {Observable<WeatherInformation>}
   * @memberof WeatherApiService
   */
  getCity(id: number): Observable<WeatherInformation> {
    return this.http
      .get<WeatherInformation>(this.URL + '/location/' + id)
      .pipe(retry(1), catchError(this.handleError));
  }


  /**
   * @name getCities
   * @description Will return all the cities that contains the given string, returns an observable to subscribe on
   *
   * @param {string} letter
   * @returns {Observable<WeatherInformation>}
   * @memberof WeatherApiService
   */
  getCities(letter: string): Observable<WeatherInformation> {
    return this.http
      .get<WeatherInformation>(this.URL + '/location/search?query=' + letter)
      .pipe(retry(1), catchError(this.handleError));
  }


  /**
   * @name handleError
   * @description custom error handler, will create an error log containing the reported error
   *
   * @param {*} error
   * @returns
   * @memberof WeatherApiService
   */
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
