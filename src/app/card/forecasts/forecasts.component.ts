import { Component, Input,Output, OnInit, EventEmitter } from '@angular/core';
import { Forecast } from '../../../interfaces/Forecast.interface';

@Component({
  selector: 'app-forecasts',
  templateUrl: './forecasts.component.html',
  styleUrls: ['./forecasts.component.scss']
})
export class ForecastsComponent implements OnInit {

  @Input() forecasts!: Forecast[]
  @Output() selectedForecast:EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    if(this.forecasts.length){
      this.forecasts.forEach(forecast => forecast.temp_ratio = this.getTempRatios(forecast))
    }
  }

  MAX_TEMP_CELCIUS: number = 56.7


  /**
   * @description visible anchors for the front to draw rules, every value represents a temperature and will be mapped the same way as the tempRatios
   *
   * @type {number[]}
   * @memberof ForecastsComponent
   */
  readonly VISIBLE_SCALES: number[] = [40,30,20]


  /**
   * @name getTempRatios
   * @description Map the min and max temperatures of a given forecast to a ratio of them compared to the highest temperature recorded
   *
   * @param {Forecast} forecast
   * @returns {{max: string, min: string}}
   * @memberof ForecastsComponent
   */
  getTempRatios(forecast: Forecast):{max: string, min: string}{

    if(forecast.max_temp>this.MAX_TEMP_CELCIUS){
      this.MAX_TEMP_CELCIUS = forecast.max_temp
    }
    /**
     * @description calculation is made to be adaptive to the max temperature, if someday we have a record of a temperature higher than the previous one then the scale will be based on this new record
     */
    const max_temp: number= Math.min(this.MAX_TEMP_CELCIUS, (forecast.max_temp / this.MAX_TEMP_CELCIUS * 100))

    /**
     * @description calculation is made to avoid negative temperature, if it's negative it will be clamped to 0 since these values are used in a chart based on height% of dom element
     */
    const min_temp: number= Math.max(0, (forecast.min_temp / this.MAX_TEMP_CELCIUS * 100))


    return {
      max: `${max_temp.toFixed(0)}%`,
      min: `${min_temp.toFixed(0)}%`,
    }
  }


  /**
   * @name selectForecast
   * @description change the current forecast to the one at the given index in the available forecasts
   *
   * @param {number} index
   * @memberof ForecastsComponent
   */
  selectForecast(index:number){
    this.selectedForecast.emit(index)
  }

}
