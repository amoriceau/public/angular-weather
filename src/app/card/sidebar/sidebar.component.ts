import { Component, Input, OnInit } from '@angular/core';
import { Forecast } from '../../../interfaces/Forecast.interface';

@Component({
  selector: 'app-card-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class CardSidebarComponent implements OnInit {
  constructor() {}
  @Input() weatherState!: { state: string; icon: string; title: string };
  @Input() forecast!: Forecast;
  ngOnInit(): void {}
}
