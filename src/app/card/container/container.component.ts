import { Component, OnInit } from '@angular/core';
import { WeatherApiService } from '../../services/weather-api/weather-api.service';
import { City } from 'src/interfaces/City.interface';
import { subscriptionMixin } from '../../helpers/mixins/subscriptionMixin.mixin';
@Component({
  selector: 'app-card-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
})
export class CardContainerComponent extends subscriptionMixin implements OnInit {
  constructor(weatherService: WeatherApiService) {
    super(weatherService)
  }

  ngOnInit(): void {
    this.static_cities.forEach((city) => this.getCityInfo(city.woeid, 'array', false));
  }


  /**
   * @description Cities that will always be shown on the homepage, their woeid is hard coded to avoid repeated api calls when we boot the application
   * @suggestion Create a job that runs once a day or week to ensure the woeid is still the same or update it if needed, yet this id is probably not subjected to any modifications
   *
   * @type {City[]}
   * @memberof CardContainerComponent
   */
  readonly static_cities: City[] = [
    { title: 'paris', woeid: 615702 },
    { title: 'bordeaux', woeid: 580778 },
    { title: 'toulouse', woeid: 628886 },
    { title: 'lyon', woeid: 609125 },
  ];
}
