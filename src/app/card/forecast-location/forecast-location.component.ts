import { Component, Input, OnInit } from '@angular/core';
import { Forecast } from 'src/interfaces/Forecast.interface';
import { WeatherInformation } from 'src/interfaces/WeatherInformation.interface';
import { Coords } from '../../../interfaces/Coords.interface';
import { sessionStorageMixin } from '../../helpers/mixins/sessionStorageMixin.mixin';

@Component({
  selector: 'app-forecast-location',
  templateUrl: './forecast-location.component.html',
  styleUrls: ['./forecast-location.component.scss'],
})
export class ForecastLocationComponent extends sessionStorageMixin implements OnInit {
  @Input() city!: WeatherInformation;
  constructor() {
    super()
  }

  ngOnInit(): void {
    if (this.city) {
      this.currentForecast = this.city.consolidated_weather[0];
      this.currentForecast.sun_rise = this.city.sun_rise;
      this.currentForecast.sun_set = this.city.sun_set;
      this.coords = this.getCoords()
    }
  }

  currentForecast!: Forecast;
  coords!:Coords

    /**
   * @name getCoords
   * @description return an object containing the geographical position of the city we're looking at
   *
   * @returns {Coords}
   * @memberof ForecastLocationComponent
   */
  getCoords(): Coords {
    const coords: string[] = this.city?.latt_long.split(',');
    return { latt: coords[0], long: coords[1] };
  }

}
