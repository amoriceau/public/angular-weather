import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CardContainerComponent } from './card/container/container.component';
import { ForecastLocationComponent } from './card/forecast-location/forecast-location.component';
import { CardSidebarComponent } from './card/sidebar/sidebar.component';
import { ForecastsComponent } from './card/forecasts/forecasts.component';
import { WeatherIconComponent } from './shared/weather-icon/weather-icon.component';
import { LocationDetailComponent } from './pages/location-detail/location-detail.component';
import { ForecastDetailsComponent } from './pages/location-detail/forecast-details/forecast-details.component';
import { ForecastContainerComponent } from './pages/location-detail/forecast-container/forecast-container.component';
import { LocationDetailHeaderComponent } from './pages/location-detail/location-detail-header/location-detail-header.component';
import { SearchbarComponent } from './navbar/searchbar/searchbar.component';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CardContainerComponent,
    ForecastLocationComponent,
    CardSidebarComponent,
    ForecastsComponent,
    WeatherIconComponent,
    LocationDetailComponent,
    ForecastDetailsComponent,
    ForecastContainerComponent,
    LocationDetailHeaderComponent,
    SearchbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
