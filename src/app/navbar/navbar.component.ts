import { Component, OnInit } from '@angular/core';
import { NavigationLink } from 'src/interfaces/NavigationLink.interface';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  tabs: NavigationLink[] = [{name: 'home', link: '/'}]

  current_tab: string = 'home'


  /**
   * @description Toggler for the dropdown menu in mobile devices
   *
   * @type {boolean}
   * @memberof NavbarComponent
   */
  expandedMenu: boolean = false


  /**
   * @name changeTab
   * @description save the current tab index (to reference a page or a component)
   *
   * @param {number} index
   * @memberof NavbarComponent
   */
  changeTab(index:number) :void{
    this.current_tab = this.tabs[index]?.name ?? this.tabs[0].name
  }


  /**
   * @name toggleMenu
   * @description Show or hide the dropdown menu on mobile devices
   *
   * @memberof NavbarComponent
   */
  toggleMenu():void{
    this.expandedMenu = !this.expandedMenu
  }

}
