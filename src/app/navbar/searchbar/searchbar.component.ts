import { Component, OnInit } from '@angular/core';
import { WeatherApiService } from 'src/app/services/weather-api/weather-api.service';
import { City } from 'src/interfaces/City.interface';
import { subscriptionMixin } from '../../helpers/mixins/subscriptionMixin.mixin';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent extends subscriptionMixin implements OnInit {
  constructor(weatherService: WeatherApiService, private router: Router) {
    super(weatherService);

    // We bind 'this' to fillSuggestion since it's used as a callback and will call the city_list from the mixin, without the bind we would have lost the reference between this 'this' and the mixin 'this'
    this.fillSuggestions.bind(this);
    this.routerSubscription = router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        (this.searchInput as HTMLTextAreaElement).value = '';
        this.suggestions = [];
      }
    });
  }

  /**
   * Initialize all the listeners for the searchbar
   */
  ngOnInit(): void {
    this.searchInput = document.getElementById('search');

    this.searchInput?.addEventListener('input', async (event) => {
      const value: string = (
        event.target as HTMLTextAreaElement
      ).value.toLowerCase();
      this.getCitiesList(value, this.fillSuggestions.bind(this));
    });
    this.searchInput?.addEventListener('keydown', (event) => {
      if (event.code === 'Enter') {
        if (this.suggestions.length) {
          this.router.navigate(['/', this.suggestions[0].woeid])
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.routerSubscription) {
       this.routerSubscription.unsubscribe();
    }
  }

  /**
   * @description It's the input element on the top menu (appbar)
   *
   * @type {(HTMLElement | null)}
   * @memberof SearchbarComponent
   */
  searchInput!: HTMLElement | null;

  /**
   * @description An array of filtered cities
   *
   * @type {City[]}
   * @memberof SearchbarComponent
   */
  suggestions: City[] = [];

  routerSubscription:Subscription

  /**
   * @name fillSuggestions
   * @description Map the cities fetched by the Weather Service to keep the cities that starts with the input (searchInput) value
   *
   * @spec This method is only used as a callback function for the mixin getCitiesList method
   * @param {string} value
   * @param {Map<string, City[]>} source
   * @returns {void}
   * @memberof SearchbarComponent
   */
  fillSuggestions(value: string, source: Map<string, City[]>): void {
    if (!value) return;
    this.suggestions =
      source
        .get(value.charAt(0))
        ?.filter((city) => city.title.toLowerCase().startsWith(value))
        .map((city) => {
          return { title: city.title, woeid: city.woeid };
        }) ?? [];
  }
}
