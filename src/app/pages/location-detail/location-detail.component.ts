import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherApiService } from '../../services/weather-api/weather-api.service';
import { WeatherInformation } from '../../../interfaces/WeatherInformation.interface';
import { subscriptionMixin } from '../../helpers/mixins/subscriptionMixin.mixin';
import { Forecast } from '../../../interfaces/Forecast.interface';
@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.scss'],
})
export class LocationDetailComponent
  extends subscriptionMixin
  implements OnInit
{
  constructor(
    weatherService: WeatherApiService,
    private route: ActivatedRoute
  ) {
    super(weatherService);
  }

  location!: WeatherInformation;
  currentForecast!: Forecast;
  currentForecastIndex: number = 0;
  loaded:boolean = false

  ngOnInit(): void {
    this.fetchData();
  }

  /**
   * @name selectForecast
   * @description Switch the current forecast to a new forecast at the index of the available forecasts
   *
   * @param {number} index
   * @memberof LocationDetailComponent
   */
  selectForecast(index: number) {
    this.currentForecast = this.city_infos.consolidated_weather[index];
    this.currentForecastIndex = index;
  }

  /**
   * @name isFirst
   * @description Is the currentForecast the first element of the available forecasts
   *
   * @returns {boolean}
   * @memberof LocationDetailComponent
   */
  isFirst(): boolean {
    return (
      this.city_infos.consolidated_weather[0].applicable_date ===
      this.currentForecast.applicable_date
    );
  }

  /**
   * @name isLast
   * @description Is the currentForecast the lase element of the available forecasts
   *
   * @returns {boolean}
   * @memberof LocationDetailComponent
   */
  isLast(): boolean {
    return (
      this.city_infos.consolidated_weather[
        this.city_infos.consolidated_weather.length - 1
      ].applicable_date === this.currentForecast.applicable_date
    );
  }

  /**
   * @name previousForecast
   * @description Set, if possible, the currentForecast as the previous one available in the forecasts list
   *
   * @memberof LocationDetailComponent
   */
  previousForecast(): void {
    if (!this.isFirst()) {
      this.selectForecast(this.currentForecastIndex - 1);
    }
  }

  /**
   * @name nextForecast
   * @description Set, if possible, the currentForecast as the next one available in the forecasts list
   *
   * @memberof LocationDetailComponent
   */
  nextForecast(): void {
    if (!this.isLast()) {
      this.selectForecast(this.currentForecastIndex + 1);
    }
  }

  /**
   * @name fetchData
   * @description subscribe to the observable given by the API service to fetch data about the city which has it's id in the param url. To slow the api consumption data will be fetched from the cache if possible, but you can force the app to fetch from the API with the `force` param
   *
   * @param {boolean} [force=false]
   * @memberof LocationDetailComponent
   */
  fetchData(force: boolean = false) {

    this.route.params.subscribe(async (params) => {
      await this.getCityInfo(+params['woeid'], 'sole', force, () => {
        this.currentForecast = this.city_infos?.consolidated_weather[0];
      });
    });
    this.loaded = true
  }
}
