import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationDetailHeaderComponent } from './location-detail-header.component';

describe('LocationDetailHeaderComponent', () => {
  let component: LocationDetailHeaderComponent;
  let fixture: ComponentFixture<LocationDetailHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationDetailHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationDetailHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
