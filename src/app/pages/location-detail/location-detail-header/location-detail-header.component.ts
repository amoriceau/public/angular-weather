import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Coords } from 'src/interfaces/Coords.interface';
import { WeatherInformation } from '../../../../interfaces/WeatherInformation.interface';
import { Forecast } from '../../../../interfaces/Forecast.interface';

@Component({
  selector: 'app-location-detail-header',
  templateUrl: './location-detail-header.component.html',
  styleUrls: ['./location-detail-header.component.scss'],
})
export class LocationDetailHeaderComponent implements OnInit {
  @Input() city_infos!: WeatherInformation;
  @Input() last_refresh!: string|null;
  @Output() refresh:EventEmitter<void> = new EventEmitter();

  constructor() {}
  coords!: Coords;
  outdatedData: boolean = false;
  todaysForecast!: Forecast

  ngOnInit(): void {}
  ngOnChanges(): void {
    this.coords = this.getCoords();
    this.todaysForecast = this.city_infos.consolidated_weather[0]

    /**
     * @description clock to watch when data might be deprecated and a refresh would be needed (30min interval)
     */
    const interval = setInterval(() => {
      if (
        new Date(
          sessionStorage.getItem('last_fetch') ??
            new Date('01/01/1970').toDateString()
        ) < new Date(Date.now() - 1000 * 60 * 30)
      ) {
        this.outdatedData = true;
      }
    }, 1000 * 60 * 30);
  }


  /**
   * @name getCoords
   * @description return an object containing the geographical position of the city we're looking at
   *
   * @returns {Coords}
   * @memberof LocationDetailHeaderComponent
   */
  getCoords(): Coords {
    const coords: string[] = this.city_infos?.latt_long.split(',');
    return { latt: coords[0], long: coords[1] };
  }


  /**
   * @name sunshine
   * @description compute an estimated sunshine duration for the current day based on the sunrise and the sunset
   *
   * @returns {Date}
   * @memberof LocationDetailHeaderComponent
   */
  sunshine():Date{
    const time =  new Date( new Date(this.city_infos.sun_set).getTime() - new Date(this.city_infos.sun_rise).getTime())
    time.setHours(time.getHours() - 1)
    return new Date(time)
  }



  /**
   * @name askRefresh
   * @description Emit a custom event to listen at from the DOM
   *
   * @memberof LocationDetailHeaderComponent
   */
  askRefresh(): void {
    this.refresh.emit()
  }
}
