import { Component, Input, OnInit } from '@angular/core';
import { Forecast } from '../../../../interfaces/Forecast.interface';

@Component({
  selector: 'app-forecast-details',
  templateUrl: './forecast-details.component.html',
  styleUrls: ['./forecast-details.component.scss']
})
export class ForecastDetailsComponent implements OnInit {

  constructor() { }
  @Input() forecast!:Forecast
  ngOnInit(): void {
  }


}
