import { Component, Input, OnInit } from '@angular/core';
import { Forecast } from '../../../../interfaces/Forecast.interface';

@Component({
  selector: 'app-forecast-container',
  templateUrl: './forecast-container.component.html',
  styleUrls: ['./forecast-container.component.scss']
})
export class ForecastContainerComponent implements OnInit {
  @Input() forecast!: Forecast
  constructor() { }

  ngOnInit(): void {
  }

}
