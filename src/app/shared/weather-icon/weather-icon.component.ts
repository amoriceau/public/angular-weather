import { Component, Input, OnInit } from '@angular/core';
import { Forecast } from 'src/interfaces/Forecast.interface';
import { iconsMap } from 'src/app/helpers/icons-map';
@Component({
  selector: 'app-weather-icon',
  templateUrl: './weather-icon.component.html',
  styleUrls: ['./weather-icon.component.scss']
})
export class WeatherIconComponent implements OnInit {

  constructor() { }
  @Input() forecast!:Forecast


  /**
   * @description Hold the weather state mapped by IconMap helper
   *
   * @type {{ state: string; icon: string; title: string }}
   * @memberof WeatherIconComponent
   */
  weatherState!:{ state: string; icon: string; title: string }

  ngOnInit(): void {}
  ngOnChanges(): void{
    this.weatherState = this.getWeatherState()
  }

  /**
   * @name getWeatherState
   * @description Map and returns the current state of the weather for a given forecast
   *
   * @returns {{ state: string; icon: string; title: string }}
   * @memberof WeatherIconComponent
   */
  getWeatherState(): { state: string; icon: string; title: string } {
    let state = this.forecast.weather_state_abbr;
    if (['lc', 'c'].includes(state)) {
      if (new Date().getHours() > 6 && new Date().getHours() < 18) {
        state += 'd';
      } else {
        state += 'n';
      }
    }
    return {
      state,
      icon: `ri-${iconsMap[state].icon}-line`,
      title: iconsMap[state].title,
    };
  }
}
