import { Forecast } from "./Forecast.interface";
import { Parent } from "./Parent.interface";
import { Source } from "./Source.interface";

export interface WeatherInformation {
  consolidated_weather: Forecast[];
  time:                 Date;
  sun_rise:             Date;
  sun_set:              Date;
  timezone_name:        string;
  parent:               Parent;
  sources:              Source[];
  title:                string;
  location_type:        string;
  woeid:                number;
  latt_long:            string;
  timezone:             string;
}




