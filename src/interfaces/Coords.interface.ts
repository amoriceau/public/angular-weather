export interface Coords {
  latt: string;
  long: string;
}
