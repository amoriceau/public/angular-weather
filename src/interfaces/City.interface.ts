export interface City {
  title: string;
  location_type?: string;
  woeid: number;
  latt_long?: string
}
